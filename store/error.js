// import errorHandlerService from '../services/errorHandler';

/**
 * Error store
 * This is the root app space
 */
export const state = () => ({
  isErrorState: false,
  message: null,
});

/**
 * https://vuex.vuejs.org/guide/actions.html
 * Actions are similar to mutations, the differences being that:
 *  - Instead of mutating the state, actions commit mutations.
 *  - Actions can contain arbitrary asynchronous operations.
 */
export const actions = {
  displayError({ commit }, { message }) {
    commit('displayError', message);
  },

  clearError({ commit }) {
    commit('clearError');
  }
};

/**
 * https://vuex.vuejs.org/guide/mutations.html
 * The only way to actually change state in a Vuex store is by committing a mutation.
 */
export const mutations = {
  clearError(state) {
    state.isErrorState = false;
    state.message = null;
  },

  displayError(state, message) {
    state.isErrorState = true;
    state.message = message;
  }
};