# Development server

This is an instance of Hasura Backend Plus (HBP).
It is the same code that is running at nhost.io and we aim to be fully portable.

# Set up
- Metadata must be migrated manually.
  - Clone [HBP repository](https://github.com/nhost/hasura-backend-plus)
  - Delete `email_templates` and `provider_requests` tables from [/db/hasura/metadata/tables.yaml#L101-L106](https://github.com/nhost/hasura-backend-plus/blob/master/db/hasura/metadata/tables.yaml#L101-L106)
  - Use hasura-cli and the following commands from [`/db/hasura`](https://github.com/nhost/hasura-backend-plus/tree/master/db/hasura)

```
To apply metadata

# apply metadata, this will connect Hasura to the configured databases.
hasura metadata apply --endpoint http://another-graphql-instance.hasura.app
# reload metadata to make sure Hasura is aware of any newly created database objects.
hasura metadata reload --endpoint http://another-graphql-instance.hasura.app

```

# Issues

- Graphql Engine and HBP depend on each other for the JWT, it should take some time to estabilize, but it is currently not working.
