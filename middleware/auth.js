export default async function(context) {
  const isLoggedIn = await context.store.dispatch('user/initialize');

  if (!isLoggedIn) {
    context.redirect('/login');
  }
}
