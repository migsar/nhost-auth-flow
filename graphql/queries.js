import gql from 'graphql-tag';

/**
 * This query is used for fetching user profile.
 * A different role is used in Hasura.
 * We already have the id, but we can fetch it.
 */
export const Profile = gql`
  query Profile {
    users {
      id
      created_at
      updated_at
      display_name
      avatar_url
    }

    auth_accounts {
      email
    }
  }
`;

/**
 * This query is used for getting a list of all users.
 * Only display_name and avatar_url are public properties.
 */
export const AllUsers = gql`
  query AllUsers {
    users {
      display_name
      avatar_url
    }
  }
`;

export default {
  Profile,
  AllUsers,
};