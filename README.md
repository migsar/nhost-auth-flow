# nhost-auth-flow

This repository is a guide for configuring a Nuxt.js application with nhost.io authentication.
We aim to provide a easy to understand and follow auth guide for front-end oriented people

Key features are:
- Fully working and secure auth flow
- Clean, commented code
- Decoupled auth code

There is a one-to-one correspondence between HBP authentication API and vuex user actions.

## Create config

```bash
create-nuxt-app v3.6.0
✨  Generating Nuxt.js project in nhost-auth-flow
? Project name: nhost-auth-flow
? Programming language: JavaScript
? Package manager: Yarn
? UI framework: Tailwind CSS
? Nuxt.js modules: (Press <space> to select, <a> to toggle all, <i> to invert selection)
? Linting tools: (Press <space> to select, <a> to toggle all, <i> to invert selection)
? Testing framework: Jest
? Rendering mode: Universal (SSR / SSG)
? Deployment target: Static (Static/Jamstack hosting)
? Development tools: jsconfig.json (Recommended for VS Code if you're not using typescript)
? Continuous integration: None
? Version control system: Git

🎉  Successfully created project nhost-auth-flow
```

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
