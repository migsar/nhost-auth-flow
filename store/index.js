/**
 * Root store
 * This is the root app space
 */
export const state = () => ({

});

/**
 * https://vuex.vuejs.org/guide/actions.html
 * Actions are similar to mutations, the differences being that:
 *  - Instead of mutating the state, actions commit mutations.
 *  - Actions can contain arbitrary asynchronous operations.
 */
export const actions = {};

/**
 * https://vuex.vuejs.org/guide/mutations.html
 * The only way to actually change state in a Vuex store is by committing a mutation.
 */
export const mutations = {};