import { createClient } from 'nhost-js-sdk';

export class NhostService {
  constructor(options) {
    // Service here is initialized with default config
    // You can overwrite by calling init
    this.init('http://localhost:5000', options);
  }

  init(baseURL, options) {
    // No baseURL validation
    // Options are forwarded to client but it could include presets
    const { auth } = createClient({
      baseURL,
      ...options,
    });

    this.client = auth;
  }

  set client(client) {
    this._client = client;
  }

  get client() {
    return this._client;
  }

  help() {
    console.debug('Nhost Js SDK Wrapper!');
  }
}

export default new NhostService({
  // autoLogin disabled because there is no way to synchronously know it succeed
  autoLogin: false,
  useCookies: true,
});