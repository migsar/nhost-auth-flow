import nhostService from '../services/nhost';
import gqlService, { GraphQLService } from '../services/graphql';
import { Profile } from '../graphql/queries';

/**
 * User store
 *
 * The purpose of this store is to implement common actions for user flow.
 * Enable user login and logout, register, etc.
 * Also it aims to define a way to propagate the changes across the application.
 */
export const state = () => ({
  isLoggedIn: false,
  user: null,
  session: null,
});

/**
 * https://vuex.vuejs.org/guide/getters.html
 * You can think of Vuex "getters" as computed properties for stores.
 */
export const getters = {
  /**
   * Convenience function to not access state outside of stores.
   */
  isUserLoggedIn(state) {
    return state.isLoggedIn;
  },
}

/**
 * https://vuex.vuejs.org/guide/actions.html
 * Actions are similar to mutations, the differences being that:
 *  - Instead of mutating the state, actions commit mutations.
 *  - Actions can contain arbitrary asynchronous operations.
 *
 * https://nhost.github.io/hasura-backend-plus/docs/api-reference#authentication
 * Actions have to implement triggers for common requests
 */
export const actions = {
  /**
   * A simple action to redirect to home after login.
   * It could be in a router store but it does not make much sense to
   * create it just for this action.
   */
  goHome() {
    this.$router.push({
      path: '/',
    });
  },

  /**
   * We try to restore user session if a refresh token is available
   * That means get the current user information and the JWT token
   * needed for doing requests to our backend.
   */
  async initialize({ commit, dispatch, state }) {
    nhostService.client.onAuthStateChanged(async (newAuthState) => {
      if (newAuthState) {
        dispatch('getUser');
      } else {
        // noop
      }
    });

    /**
     * Called without any args
     */
    nhostService.client.onTokenChanged(() => {
      // noop
    });

    try {
      await nhostService.client.refreshSession();
      return Boolean(state.session?.token);
    } catch(error) {
      return false;
    }
  },

  async getProfile({ state }) {
    const client = gqlService.client;
    const res = await client.request(Profile, {}, {
      [GraphQLService.Strings.Authorization]: `Bearer ${state.session.token}`,
      [GraphQLService.Strings.HasuraHeaderRole]: GraphQLService.Strings.HasuraRoleMe,
    });

    // The property users depends on the query being used to retrieve data.
    return {
      ...res.users[0],
      ...res.auth_accounts[0],
    };
  },

  /**
   * Retrieve user.
   * It is used for restoring user's session when a refresh token is available.
   */
  async getUser({ commit, dispatch }) {
    const user = await nhostService.client.user();
    const token = await nhostService.client.getJWTToken();

    dispatch('error/clearError', null, { root: true });
    commit('setUser', user);
    commit('setSession', {
      token,
      // Apparently nhost library does not return that
      expiresInMs: 900000,
    });
},

  /**
   * Register a new user
   * If there is no email validation the user is signed in
   */
  async signUp({ commit, dispatch }, { username: email, password }) {
    try {
      const { session: { jwt_token, jwt_expires_in }, user } = await nhostService.client.register({ email, password });

      dispatch('error/clearError', null, { root: true });
      commit('setUser', user);
      commit('setSession', {
        token: jwt_token,
        expiresInMs: jwt_expires_in,
      });
      dispatch('goHome');
    } catch(error) {
      const { message } = error.response.data;

      dispatch('error/displayError', { message }, { root: true });
      commit('clearSession');
      commit('clearUser');
    }
  },

  async logIn({ commit, dispatch }, { username: email, password }) {
    try {
      const { session: { jwt_token, jwt_expires_in }, user } = await nhostService.client.login({ email, password });

      dispatch('error/clearError', null, { root: true });
      commit('setUser', user);
      commit('setSession', {
        token: jwt_token,
        expiresInMs: jwt_expires_in,
      });
      dispatch('goHome');
    } catch(error) {
      const { message } = error.response.data;

      dispatch('error/displayError', { message }, { root: true });
      commit('clearSession');
      commit('clearUser');
    }
  },

  async logOut({ commit }) {
    await nhostService.client.logout();
    commit('clearUser');
    commit('clearSession');
  },

  // To be implemented
  // activateAccount() {},

  // To be implemented
  // deleteAccount() {},

  // To be implemented
  // User is logged in
  // changePassword() {},

  // To be implemented
  // User is logged in
  // changePasswordRequest() {},

  // To be implemented
  // changePasswordConfirm() {},

  // To be implemented
  // User is logged in
  // changeEmail() {},

  // To be implemented
  // User is logged in
  // changeEmailRequest() {},

  // To be implemented
  // changeEmailConfirm() {},

  // To be implemented
  // refreshToken() {},

  // To be implemented
  // revokeTokens() {},
};

/**
 * https://vuex.vuejs.org/guide/mutations.html
 * The only way to actually change state in a Vuex store is by committing a mutation.
 */
export const mutations = {
  clearUser(state) {
    state.isLoggedIn = false;
    state.user = null;
  },

  setUser(state, { id, display_name: displayName, email}) {
    state.isLoggedIn = true;
    state.user = {
      id,
      displayName,
      email,
    };
  },

  clearSession(state) {
    state.session = null;
  },

  setSession(state, { token, expiresInMs }) {
    state.session = {
      token,
      createdAt: Date.now(),
      expiresInMs,
    };
  }
};