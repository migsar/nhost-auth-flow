// https://github.com/prisma-labs/graphql-request
import { GraphQLClient } from 'graphql-request';

export const Strings = {
  Authorization: 'Authorization',
  HasuraHeaderUserId: 'x-hasura-user-id',
  HasuraHeaderRole: 'x-hasura-role',
  HasuraRoleUser: 'user',
  HasuraRoleMe: 'me',
};

export class GraphQLService {
  constructor(baseUrl, options = {}) {
    // Service here is initialized with default config
    // You can overwrite by calling init
    this.init(baseUrl, options);
  }

  init(baseUrl, options = {}) {
    // No baseUrl validation
    // Options are forwarded to client but it could include presets
    const client = new GraphQLClient(baseUrl, options)
    this.client = client;
  }

  set client(client) {
    this._client = client;
  }

  get client() {
    return this._client;
  }

  help() {
    console.debug('This is a graphql-request wrapper!');
  }
}

GraphQLService.Strings = Strings;

export default new GraphQLService('http://localhost:8080/v1/graphql');